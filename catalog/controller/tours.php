<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class tours {

	public function gettours(Request $request, Application $app){

		$program = new \model\tours;
		$tours = $program->GetAllTours($app);

		foreach ($tours as $kay => $tour){

			$twig['tours'][$kay]['title'] = $tour['title'];
			$twig['tours'][$kay]['desc'] = $tour['desc'];

			if (isset($tour['photo']) && !empty($tour['photo'])){
				$twig['tours'][$kay]['photo'] = $tour['photo'];
			}else{
				$twig['tours'][$kay]['photo'] = SITE_NAME.'image/no_image.png';
			}

		}

		return $app['twig']->render('tours.twig',$twig);

		}

}