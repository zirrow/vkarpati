<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class order {

	public function getorder(Application $app){

		$twig['pay_id'] = date_timestamp_get(date_create());
		$twig['paymetods'] = $this->GetPayMetods();


		return $app['twig']->render('order.twig',$twig);

	}

	public function GetPayMetods (){

		$postdata = http_build_query(
			array(
				'ik_co_id' => '57c6b2943c1eaf5d4f8b4573',
				'ik_pm_no' => date_timestamp_get(date_create()),
				'ik_am' => '129',
				'ik_desc' => 'Запрос вариантов оплаты',
				'ik_act' => 'payways',
				'ik_int' => 'json'
			)
		);

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents('https://sci.interkassa.com/', false, $context);

		$xml_pays = json_decode($result, true);

		$result = file_get_contents('https://api.interkassa.com/v1/paysystem-input-payway');

		$xml_desc = json_decode($result, true);


		/* $xml_pays = als curAls
		 *
		 * $xml_desc = name[]
		 *
		 *
		 * */

		foreach ($xml_pays['resultData']['paywaySet'] as $key){

			foreach ($xml_desc['data'] as $key2){

				if($key['_id'] == $key2['_id']){

					$payMetodsArr[] = array(
						'als'    => $key['als'],
						'curAls' => $key['curAls'],
						'name'   => $key2['name']
					) ;

				}

			}

		}

		return $payMetodsArr;
	}



}