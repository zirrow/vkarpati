<?php
require_once __DIR__ . '/../config.php';

require_once __DIR__.'/../vendor/autoload.php';

//подключаем  Silex и передаем в него какие-то конфиги
$app = new Silex\Application();

//подключаем графический интерпритатор

$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => DIR_CATALOG.'view/',
));

//подключаем переводчик
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
	'locale_fallbacks' => array('ru'),
));

use Symfony\Component\Translation\Loader\YamlFileLoader;

$app->extend('translator', function($translator, $app) {
	$translator->addLoader('yaml', new YamlFileLoader());

	$translator->addResource('yaml', DIR_CATALOG.'locales/ru.yml', 'ru');
	$translator->addResource('yaml', DIR_CATALOG.'locales/en.yml', 'en');


	return $translator;
});

//подключаем и настраиваем базу данных
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array (
			'driver'    => 'pdo_mysql',
			'host'      => DB_HOST,
			'dbname'    => DB_NAME,
			'user'      => DB_USER,
			'password'  => DB_PASS,
			'charset'   => 'utf8mb4',
	),
));

//Пишем роуты и присваиваем им имена.
$app->get('/', 'controller\index::index')->bind('homepage');

$app->get('/about.html', 'controller\about::getabout')->bind('about');

$app->get('/order.html', 'controller\order::getorder')->bind('order');

$app->get('/tours.html', 'controller\tours::gettours')->bind('tours');


$app['debug'] = true;

$app->run();